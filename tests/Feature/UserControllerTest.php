<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
  use DatabaseTransactions;

  public function setUp(): void
  {
    $_ENV['APP_URL'] = 'http://localhost/api/v1';
    parent::setUp();
    $this->withHeaders([
      'Accept' => 'application/json'
    ]);
  }

  /**
   * A basic test example.
   *
   * @return void
   */
  public function testCreateUser()
  {
    User::factory(5)->create(['type' => 'guardian']);
    User::factory(5)->create(['type' => 'student']);

    $this->assertTrue(User::all()->count() == 10);
  }

  public function testCreateTokenForGuardian()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $response = $this->post('/tokens/create/' . $user->uuid, [
      'type' => 'login'
    ]);

    $response->assertStatus(200)
      ->assertSee(['token']);
  }

  public function testGuardianLogin()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $response = $this->post('/login', [
      'email' => $user->email,
      'password' => 'password'
    ]);

    $response->assertStatus(200)
      ->assertSee('Login berjaya!');
  }

  public function testSignUp()
  {
    $email = 'testsignup@gmail.com';
    $response = $this->post('/signup', [
      'email' => $email,
      'password' => 'password',
      'type' => 'guardian',
      'name' => 'Hanzoriki'
    ]);

    $response->assertStatus(200);

    $this->assertTrue(User::where('email', $email)->exists());
  }

  public function testGuardianLogout()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $response = $this->actingAs($user)->post('/logout/' . $user->uuid);

    $response->assertStatus(200)
      ->assertSee('Logout berjaya');
  }

  public function testVerifyByEmail()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $response = $this->post('/verify/email', [
      'email' => $user->email,
    ]);

    $response->assertStatus(200)
      ->assertSee(['verified' => 'true']);
  }

  public function testRequestForgotPassword()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $response = $this->post('/forgot/request', [
      'email' => $user->email,
    ]);

    $response->assertStatus(200)
      ->assertSee(['token']);
  }

  public function testChangePassword()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $token = Crypt::encryptString($user->uuid);

    $response = $this->post('/forgot/changePassword', [
      'token' => $token,
      'password' => 'secret'
    ]);

    $response->assertStatus(200)
      ->assertSee(['message' => 'Password berjaya ditukar! Sila login dengan password baru anda']);

    // verify password is really change
    $this->assertFalse(Hash::check('password', User::f($user->uuid)->password));
  }

  public function testUpdateProfile()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $token = Crypt::encryptString($user->uuid);
    $file = UploadedFile::fake()->image('avatar.jpeg');
    $response = $this->actingAs($user)->post('/users/' . $user->uuid, [
      'name' => 'Genji',
      'avatar' => $file,
      '_method' => 'PUT'
    ]);

    $response->assertStatus(200)
      ->assertSee(['message' => 'Berjaya dikemaskini', 'user']);

    // verify password is really change
    $this->assertTrue(Storage::disk('s3')->exists($file->getFilename()));

    // delete the file
    Storage::disk('s3')->delete($user->avatar);

    // verify user name is change
    $this->assertTrue($user->name == 'Genji');

    $this->assertFalse(Storage::disk('s3')->exists($file->getFilename()));
  }

  public function testGetUsers()
  {
    $user = User::factory()->create(['type' => 'guardian']);

    User::factory(55)->create(['type' => 'guardian']);

    $response = $this->actingAs($user)->get('/users');

    $response->assertStatus(200)
      ->assertSee(['data']);

    $this->assertTrue(User::count() == 56);
  }

  public function testFilterByName()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    User::factory(10)->create(['type' => 'guardian']);
    User::factory()->create(['type' => 'guardian', 'name' => 'hayabusa']);

    $response = $this->actingAs($user)->get('/users?name=hayabusa');

    $response->assertStatus(200)
      ->assertSee(['data']);

    $this->assertTrue($response->getOriginalContent()['data']->toArray()['total'] == 1);
  }

  public function testDeleteUser()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $userToDelete = User::factory()->create(['type' => 'guardian', 'name' => 'i am to delete']);
    $response = $this->actingAs($user)->delete('/users/' . $userToDelete->uuid);
    $response->assertOk()->assertSee(['message' => 'User dibuang']);

    $currentUserToDelete = \DB::table('users')
      ->where('uuid', $userToDelete->uuid)
      ->first();

    $this->assertNotNull($currentUserToDelete->deleted_at);
  }

  public function testShowUser()
  {
    $user = User::factory()->create(['type' => 'guardian']);
    $userToShow = User::factory()->create(['type' => 'guardian', 'name' => 'i am to show']);
    $response = $this->actingAs($user)->get('/users/' . $userToShow->uuid);
    $response->assertOk();

    $this->assertTrue($response->getOriginalContent()['data']->toArray()['name'] == 'i am to show');
  }
}
