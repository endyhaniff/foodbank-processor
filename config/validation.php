<?php

return [
  'auth' => [
    'login' => [
      'email' => 'required|email',
      'password' => 'required|string|min:6'
    ],
    'signup' => [
      'email' => 'required|email',
      'password' => 'required|string|min:6',
    ],
    'verify' => [
      'email' => ['email' => 'required|email']
    ],
    'forgot' => [
      'verify' => [
        'token' => 'required|string',
        'password' => 'required|string|min:6'
      ]
    ]
  ],
  'user' => [
    'update' => []
  ]
];
