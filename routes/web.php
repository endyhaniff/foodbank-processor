<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::group(['middleware' => 'throttle:1,.3'], function () {
  Route::post('/upload', 'MainController@upload')->name('upload');
});
Route::post('/download', 'MainController@download')->name('download');
