<?php

use Illuminate\Support\Facades\Route;

Route::group([''], function ($router) {
  $router->post('/tokens/create/{uuid}', 'UserController@createToken');
  $router->post('/signup', 'UserController@signUp');
  $router->post('/login', 'UserController@login');
  $router->post('/verify/email', 'UserController@verifyUsingEmail');
  $router->post('/forgot/request', 'UserController@requestForgot');
  $router->post('/forgot/changePassword', 'UserController@changePassword');
  $router->group(['middleware' => 'auth:sanctum'], function ($router) {
    $router->post('/logout/{uuid}', 'UserController@logout');
    $router->resource('users', 'UserController');
  });
});
