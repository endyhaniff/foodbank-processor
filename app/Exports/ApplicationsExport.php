<?php

namespace App\Exports;

use App\Models\Application;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ApplicationsExport implements FromView, WithHeadings
{

  public function view(): View
  {
    return view('export', [
      'applicants' => Application::orderBy('timestamp', 'asc')->get()
    ]);
  }

  public function headings(): array
  {
    return [
      "FBD ID",
      "Timestamp",
      "Email Address",
      "Full Name / Nama Penuh",
      "Mobile No",
      "Address (Unit, Block, Jalan, Taman)",
      "City",
      "Postcode",
      "State",
      "Batch",
      "Remarks",
      "Status",
      "Duplicate Phone",
      "Duplicate IC",
      "Processed IC",
      "IC",
      "Message",
      "Duplicate Address",
      "Processed Phone",
      "Processed Address",
    ];
  }
}
