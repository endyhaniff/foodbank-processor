<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\UnauthorizedException;

class UserController extends Controller
{
  public function createToken(Request $request, $id)
  {
    $user = User::f($id) ?? abort(422, __('User tidak wujud'));
    $token = $user->createToken($request->has('type') ? $request->type : 'login');

    return response()->json([
      'token' =>  $token->plainTextToken
    ]);
  }

  public function signUp(Request $request)
  {
    $request->validate(config('validation.auth.signup'));
    $data = $request->except(['password']);

    !User::where('email', $request->email)->first() ?? abort(422, __('User dengan email ini telah wujud'));

    $user = User::create(array_merge($data, ['password' => bcrypt($request->password)]));

    return response()->json([
      'message' => 'User berjaya signup',
      'data' => $user
    ]);
  }

  public function login(Request $request)
  {
    $request->validate(config('validation.auth.login'));

    $credentials = $request->only(['password', 'email']);

    if ($this->attempt($credentials)) {
      $user = User::where('email', $credentials['email'])->first();
      $token = $user->createToken('login');

      return response()->json([
        'message' => __('Login berjaya!'),
        'token' =>  $token->plainTextToken,
        'data' => $user
      ]);
    }
    throw new UnauthorizedException('Pastikan email dan password anda betul');
  }

  public function attempt($credentials)
  {
    $user = User::where('email', $credentials['email'])->first();

    if ($user) {
      return Hash::check($credentials['password'], $user->password);
    }
    return false;
  }

  public function logout($uuid)
  {
    $user = User::f($uuid) ?? abort(422, __('User not found'));
    $token = $user->tokens()->delete();

    return response()->json([
      'message' => __('Logout berjaya!')
    ]);
  }

  public function verifyUsingEmail(Request $request)
  {
    $request->validate(config('validation.auth.verify.email'));

    User::where('email', '=', $request->email)->first() ?? abort(422, __('Email tidak wujud'));

    return [
      'verified' => true
    ];
  }

  public function requestForgot(Request $request)
  {
    $request->validate(config('validation.auth.verify.email'));

    $user = User::where('email', '=', $request->email)->first() ?? abort(422, __('Email tidak wujud'));

    return [
      'token' => Crypt::encryptString($user->uuid)
    ];
  }

  public function changePassword(Request $request)
  {
    $request->validate(config('validation.auth.forgot.verify'));

    $user_id = Crypt::decryptString($request->token);

    $user = User::f($user_id) ?? abort(422, __('User tidak wujud'));

    $user->password = bcrypt($request->password);
    $user->save();

    return [
      'message' => __('Password berjaya ditukar! Sila login dengan password baru anda')
    ];
  }

  public function index(Request $request)
  {
    $from = $request->has('from') ? $request->from : Carbon::now()->startOfYear()->toDateString();
    $to = $request->has('to') ? $request->from : Carbon::now()->endOfYear()->addDays(1)->toDateString();

    $list = User::whereBetween('created_at', [$from, $to])
      ->when($request->has('name'), fn ($q) => $q->where('name', 'like', $request->name))
      ->when($request->has('email'), fn ($q) => $q->where('email', '=', $request->email))
      ->when($request->has('state'), fn ($q) => $q->where('state', '=', $request->state))
      ->when($request->has('postcode'), fn ($q) => $q->where('postcode', '=', $request->postcode))
      ->when($request->has('gender'), fn ($q) => $q->where('gender', '=', $request->gender))
      ->when($request->has('type'), fn ($q) => $q->where('type', '=', $request->type))
      ->paginate(config('boilerplate.default_pagination'));

    return response()->json([
      'data' => $list
    ]);
  }

  public function update(Request $request)
  {
    $request->validate(config('validation.user.update'));
    $data = $request->except(['avatar']);
    $user = auth()->user();

    if ($request->hasFile('avatar')) {
      $file = $request->file('avatar');
      $path = Storage::disk('s3')->put($file->getFilename(), $file, 'public');

      $user->avatar = $path;
      $user->save();
    }

    $user->update($data);

    return response()->json([
      'message' => 'Berjaya dikemaskini',
      'user' => $user
    ]);
  }

  public function show($id)
  {
    $user = User::f($id) ?? abort(422, __('User tidak wujud'));

    return [
      'data' => $user
    ];
  }

  public function destroy($id)
  {
    $user = User::f($id) ?? abort(422, __('User tidak wujud'));

    $user->delete();

    return [
      'message' => 'User dibuang'
    ];
  }
}
