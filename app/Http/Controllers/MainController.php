<?php

namespace App\Http\Controllers;

use App\Exports\ApplicationsExport;
use App\Imports\ApplicationsImport;
use App\Jobs\InsertApplicationData;
use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Document;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\DB;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Propaganistas\LaravelPhone\PhoneNumber;

class MainController extends Controller
{
  public function index()
  {
    return view('index');
  }

  public function upload(Request $request)
  {
    try {
      $request->validate([
        'upload' => 'required|file|mimes:csv,xls,xlsx',
      ]);
      // $file = $request->file('upload');
      // $path = $file->store('applications', 's3');
      // Document::create([
      //   'filename' => $file->getClientOriginalName(),
      //   'path' => $path,
      //   'size' => $file->getSize()
      // ]);

      $rows = Excel::toCollection(new ApplicationsImport, $request->file('upload'), 'UTF-8');

      $rows = $rows->first();
      $dataToInsert = collect();

      if ($rows) {
        $processedData = $rows->map(function ($row, $i) {
          return $this->processData($row, $i);
        });
        $dataToInsert = array_merge([], $processedData->toArray());
        $dupeAddresses = $processedData->duplicates('stripped_address');
        $dupePhones = $processedData->duplicates('processed_phone');
        $dupeIcs = $processedData->duplicates('processed_ic');
        foreach ($dupeAddresses as $key => $dupeAddress) {
          $dataToInsert[$key]['duplicate_address'] = 1;
          $dataToInsert[$key]['status'] = 'rejected';
          $dataToInsert[$key]['remarks'] = 'Duplicate address';
        }
        foreach ($dupePhones as $key => $dupePhone) {
          $dataToInsert[$key]['duplicate_phone'] = 1;
          $dataToInsert[$key]['status'] = 'rejected';
          $dataToInsert[$key]['remarks'] .= ' Duplicate phone';
        }
        foreach ($dupeIcs as $key => $dupeIc) {
          $dataToInsert[$key]['duplicate_ic'] = 1;
          $dataToInsert[$key]['status'] = 'rejected';
          $dataToInsert[$key]['remarks'] .= ' Duplicate IC';
        }

        foreach ($dataToInsert as $key => $data) {
          if ($data['duplicate_phone'] != 1 || $data['duplicate_address'] != 1) {
            if (Carbon::parse($data['timestamp'])->tz('Asia/Kuala_Lumpur')->diffInDays(now()) > 30) {
              $dataToInsert[$key]['status'] = 'approved';
              $dataToInsert[$key]['remarks'] = 'Application after 30 days';
            }
          }
        }
      }

      DB::beginTransaction();
      InsertApplicationData::dispatch(collect($dataToInsert));
      DB::commit();
    } catch (\Exception $e) {
      dd($e);
      DB::rollback();
      $file = $request->file('upload');
      $doc = Document::where('filename', $file->getClientOriginalName())->first();
      if ($doc) {
        Storage::disk('s3')->delete($doc->path);
        $doc->delete();
      }
      if ($e instanceof \Illuminate\Validation\ValidationException) {
        return back()->withErrors($e->errors());
      }
      return back()->withErrors($e->getMessage());
    }

    return back()->with('message', 'Successfully uploaded');
  }

  public function processData($row, $i)
  {
    $phone = $this->sanitizePhone($row['mobile_no']);

    $newData = [
      'fbd_id' => $row['fbd_id'],
      'timestamp' => $row['timestamp'],
      'email' => $row['email_address'],
      'fullname' => $row['full_name_nama_penuh'],
      'mobile' => $row['mobile_no'],
      'processed_phone' => preg_replace('/[\+]+/', '', $row['mobile_no']),
      'address' => $row['address_unit_block_jalan_taman'],
      'ic' => $row['no_kad_kad_pengenalan'],
      'message' => $row['message_mesej'],
      'city' => $row['city'],
      'postcode' => $row['postcode'],
      'state' => $row['state'],
      'batch' => $row['batch'],
      'remarks' => $row['remarks'],
      'status' => $row['status'],
    ];
    $timezone = new DateTimeZone('Asia/Kuala_Lumpur');
    if (gettype($row['timestamp']) == 'string') {
      try {
        $timestamp = Date::dateTimeToExcel(Carbon::parse($row['timestamp'], $timezone)->toDateTime());
      } catch (\Exception $e) {
        $timestamp = $row['timestamp'];
      }
    } else {
      $timestamp = $row['timestamp'];
    }
    $newData['timestamp'] = Date::excelToDateTimeObject($timestamp, $timezone)->format('m/d/Y H:i:s');
    $newData['stripped_address'] = $this->stripAddress($row['address_unit_block_jalan_taman']);
    $phoneLib = PhoneNumberUtil::getInstance();
    $newData['processed_ic'] = $this->stripAddress($newData['ic']);
    if (!empty($phone)) {
      try {
        $proto  = $phoneLib->parse($phone, 'MY');
        $newData['processed_phone'] = $phoneLib->format($proto, PhoneNumberFormat::INTERNATIONAL);
      } catch (\Exception $e) {
        $newData['processed_phone'] = $row['mobile_no'];
        $newData['remarks'] = $newData['remarks'] . ' | ERROR Phone number is not valid';
      }
    } else {
      $newData['remarks'] = $newData['remarks'] . ' | ERROR Phone number is not valid';
    }

    $newData['status'] = 'approved';
    $newData['duplicate_phone'] = 0;
    $newData['duplicate_address'] = 0;
    $newData['duplicate_ic'] = 0;
    $newData['processed_at'] = now()->tz('Asia/Kuala_Lumpur');

    return $newData;
  }

  public function stripAddress($address)
  {
    $stripAddress = preg_replace('/[ ,-]+/', '', $address);
    $stripAddress = mb_substr($stripAddress, 0, 100, 'UTF-8');
    return $stripAddress;
  }

  public function sanitizePhone($data)
  {
    $phone = $data;
    if (strlen($data) > 16) {
      $phone = explode(' ', $data)[0];
      $phone = explode('/', $phone)[0];
    }
    $phone = preg_replace('/[^0-9.]+/', '', $phone);
    $phone = preg_replace('/[\+]/', '', $phone);

    return $phone;
  }

  public function download()
  {
    return Excel::download(new ApplicationsExport, 'Apply Foodbank.Digital (Response)' . now()->tz('Asia/Kuala_Lumpur')->format('Y_md g:i A') . '.xlsx');
  }
}
