<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait Uuids
{
  public static function boot()
  {
    parent::boot();
    self::creating(function ($model) {
      $model->uuid = (string) Str::uuid();
    });
  }

  public function scopeF($q, $uuid)
  {
    return $q->where('uuid', $uuid)->first();
  }

  public function getRouteKeyName()
  {
    return 'uuid';
  }
}
