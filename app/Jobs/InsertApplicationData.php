<?php

namespace App\Jobs;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InsertApplicationData implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  protected $dataToInsert;
  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($dataToInsert)
  {
    $this->dataToInsert = $dataToInsert;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $chunks = $this->dataToInsert->chunk(300);
    foreach ($chunks as $chunk) {
      $datas = $chunk->toArray();
      foreach ($datas as $data) {
        Application::insert(array_merge($data, ['created_at' => now(), 'updated_at' => now()]));
      }
    }
  }
}
