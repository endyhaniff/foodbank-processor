<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
  protected $guarded = ['id'];
  protected $dates = ['created_at'];
  protected $hidden = ['id'];

  use HasFactory;
}
