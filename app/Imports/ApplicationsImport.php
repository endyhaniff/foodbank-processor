<?php

namespace App\Imports;

use App\Models\Application;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ApplicationsImport implements ToModel, WithStartRow, WithHeadingRow
{
  /**
   * @param array $row
   *
   * @return \Illuminate\Database\Eloquent\Model|null
   */
  public function model(array $row)
  {
    return new Application([
      'fbd_id' => $row['fbd_id'],
      'timestamp' => $row['timestamp'],
      'email' => $row['email_address'],
      'fullname' => $row['full_name_nama_penuh'],
      'mobile' => $row['mobile_no'],
      'address' => $row['address_unit_block_jalan_taman'],
      'city' => $row['city'],
      'postcode' => $row['postcode'],
      'state' => $row['state'],
      'batch' => $row['batch'],
      'remarks' => $row['remarks'],
      'status' => $row['status'],
      'message' => $row['message_mesej'],
      'ic' => $row['no_kad_kad_pengenalan'],
    ]);
  }

  public function startRow(): int
  {
    return 2;
  }
}
