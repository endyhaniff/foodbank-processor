<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMessageAndNoicToApplications extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('applications', function (Blueprint $table) {
      $table->string('message')->after('processed_at')->nullable();
      $table->string('ic')->after('processed_at')->nullable();
      $table->string('processed_ic')->after('processed_at')->nullable();
      $table->boolean('duplicate_ic')->after('processed_at')->default(0);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('applications', function (Blueprint $table) {
      //
    });
  }
}
