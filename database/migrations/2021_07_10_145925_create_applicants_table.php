<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('applicants', function (Blueprint $table) {
      $table->id();
      $table->string('email')->nullable();
      $table->string('fullname')->nullable();
      $table->string('mobile')->nullable();
      $table->string('address')->nullable();
      $table->string('city')->nullable();
      $table->string('postcode')->nullable();
      $table->string('state')->nullable();
      $table->string('batch')->nullable();
      $table->string('remarks')->nullable();
      $table->string('status')->nullable();
      $table->string('stripped_address')->nullable();
      $table->string('timestamp')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('applicants');
  }
}
