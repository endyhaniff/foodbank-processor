<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDuplicatePhoneDuplicateAddressToApplicant extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('applicants', function (Blueprint $table) {
      $table->string('processed_phone')->after('timestamp');
      $table->boolean('duplicate_phone')->default(0)->after('timestamp');
      $table->boolean('duplicate_address')->default(0)->after('timestamp');
      $table->datetime('processed_at')->after('timestamp');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('applicant', function (Blueprint $table) {
      //
    });
  }
}
