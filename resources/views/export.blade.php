<table>
  <thead>
    <tr>
      <th>FBD ID</th>
      <th>Timestamp</th>
      <th>Email Address</th>
      <th>Full Name / Nama Penuh</th>
      <th>Mobile No</th>
      <th>Address (Unit, Block, Jalan, Taman)</th>
      <th>City</th>
      <th>Postcode</th>
      <th>State</th>
      <th>Batch</th>
      <th>Remarks</th>
      <th>Message Mesej</th>
      <th>No Kad Kad Pengenalan</th>
      <th>Status</th>
      <th>Duplicate Phone</th>
      <th>Duplicate Address</th>
      <th>Duplicate IC</th>
      <th>Processed Phone</th>
      <th>Processed IC</th>
      <th>Processed Address</th>
      <th>Processed At</th>
    </tr>
  </thead>
  <tbody>
    @foreach($applicants as $applicant)
    <tr>
      <td>{{ $applicant->fbd_id }}</td>
      <td>{{ $applicant->timestamp }}</td>
      <td>{{ $applicant->email }}</td>
      <td>{{ $applicant->fullname }}</td>
      <td>{{ $applicant->mobile }}</td>
      <td>{{ $applicant->address }}</td>
      <td>{{ $applicant->city }}</td>
      <td>{{ $applicant->postcode }}</td>
      <td>{{ $applicant->state }}</td>
      <td>{{ $applicant->batch }}</td>
      <td>{{ $applicant->remarks }}</td>
      <td>{{ $applicant->message }}</td>
      <td>{{ $applicant->ic }}</td>
      <td>{{ $applicant->status }}</td>
      <td>{{ $applicant->duplicate_phone ? 'true' : 'false' }}</td>
      <td>{{ $applicant->duplicate_address ? 'true' : 'false' }}</td>
      <td>{{ $applicant->duplicate_ic ? 'true' : 'false' }}</td>
      <td>{{ $applicant->processed_phone }}</td>
      <td>{{ $applicant->processed_ic }}</td>
      <td>{{ $applicant->stripped_address }}</td>
      <td>{{ $applicant->processed_at }}</td>
    </tr>
    @endforeach
  </tbody>
</table>