<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
  <style>
    body {
      font-family: 'Nunito', sans-serif;
    }

    .loader div {
      animation-duration: 0.5s;
    }

    .loader div:first-child {
      animation-delay: 0.1s;
    }

    .loader div:nth-child(2) {
      animation-delay: 0.3s;
    }

    .loader div:nth-child(3) {
      animation-delay: 0.6s;
    }
  </style>
  <title>Foodbank</title>
</head>

<body>
  <div class="wrapper w-screen h-screen bg-gray-300">
    <main id="main" class="h-full w-full " style="display:none">
      <div class="flex h-full w-full  items-center justify-center">
        <div id="dismissDiv" style="{{Session::has('message') ? 'display:flex' : 'display:none'}}" class="bg-[#3cc1ac] text-white fixed right-5 top-5 rounded-lg px-4 py-1">
          <svg id="dismiss" class="fill-current cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20">
            <path fill="none" d="M0 0h24v24H0z" />
            <path d="M12 10.586l4.95-4.95 1.414 1.414-4.95 4.95 4.95 4.95-1.414 1.414-4.95-4.95-4.95 4.95-1.414-1.414 4.95-4.95-4.95-4.95L7.05 5.636z" />
          </svg>
          <div>
            @if(Session::has('message'))
            Successfully uploaded 😄
            @endif
          </div>
        </div>
        <div id="errorDiv" style="{{$errors->any() ? 'display:flex' : 'display:none'}}" class="errors bg-red-500 text-white fixed right-5 top-5 rounded-lg px-4 py-1">
          <svg id="closeError" class="fill-current cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20">
            <path fill="none" d="M0 0h24v24H0z" />
            <path d="M12 10.586l4.95-4.95 1.414 1.414-4.95 4.95 4.95 4.95-1.414 1.414-4.95-4.95-4.95 4.95-1.414-1.414 4.95-4.95-4.95-4.95L7.05 5.636z" />
          </svg>
          @if($errors->any())
          @foreach ($errors->all() as $error)
          <div class="error">{{$error}}</div>
          @endforeach
          @endif
        </div>
        <div class="flex justify-center items-center flex-col">
          <div class="logo w-2/6 mb-4">
            <img src="/foodbank-logo.png" width="100%" alt="">
          </div>
          <div id="loader" style="display:none" class="loader rounded-full flex space-x-3">
            <div class="w-5 h-5 bg-[#3cc1ac] rounded-full animate-bounce"></div>
            <div class="w-5 h-5 bg-[#3cc1ac] rounded-full animate-bounce"></div>
            <div class="w-5 h-5 bg-[#3cc1ac] rounded-full animate-bounce"></div>
          </div>
          <div id="button-group" style="display:flex" class=" space-x-4">
            <form enctype="multipart/form-data" method="POST" id="upload" action="{{route('upload')}}">
              <label class="bg-[#3cc1ac] disabled:bg-[#67bfb1] hover:shadow-none flex text-white px-4 py-2 rounded cursor-pointer hover:shadow-lg" for="uploadselect">
                <svg class="fill-current mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                  <path fill="none" d="M0 0h24v24H0z" />
                  <path d="M15 4H5v16h14V8h-4V4zM3 2.992C3 2.444 3.447 2 3.999 2H16l5 5v13.993A1 1 0 0 1 20.007 22H3.993A1 1 0 0 1 3 21.008V2.992zM13 12v4h-2v-4H8l4-4 4 4h-3z" />
                </svg>
                Upload File</label>
              <input style="display:none" id="uploadselect" name="upload" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
              @csrf
            </form>
            <form method="POST" id="download" action="{{route('download')}}">
              <button id="downloadbtn" type="submit" class="bg-[#ff6600] disabled:bg-[#f5a571] hover:shadow-none flex text-white px-4 py-2 rounded cursor-pointer hover:shadow-lg">
                <svg class="mr-2 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                  <path fill="none" d="M0 0h24v24H0z" />
                  <path d="M13 10h5l-6 6-6-6h5V3h2v7zm-9 9h16v-7h2v8a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-8h2v7z" />
                </svg>
                Download</button>
              @csrf
            </form>
          </div>
        </div>
      </div>
    </main>
  </div>
  <script src="https://unpkg.com/tailwindcss-jit-cdn"></script>
  <script>
    setTimeout(function() {
      let main = document.getElementById('main');
      if (main) {
        main.style.display = 'block';
      }
    }, 500)

    let uploadForm = document.querySelector('#upload');
    let uploadInput = document.querySelector('input[name="upload"]');
    let errorDiv = document.querySelector('#errorDiv')
    let closeError = document.querySelector('#closeError')
    let downloadBtn = document.querySelector('#downloadbtn')
    let dismiss = document.querySelector('#dismiss')
    let dismissDiv = document.querySelector('#dismissDiv')
    let loader = document.querySelector('#loader')
    let buttongroup = document.querySelector('#button-group')

    function hideEl(el) {
      el.style.display = 'none'
    }

    closeError.addEventListener('click', (e) => {
      e.stopPropagation()
      hideEl(errorDiv)
    });

    dismiss.addEventListener('click', (e) => {
      e.stopPropagation()
      hideEl(dismissDiv)
    });

    uploadInput.addEventListener('change', function() {
      uploadForm.submit();
      uploadInput.disabled = true;
      downloadBtn.disabled = true;
      loader.style.display = 'flex';
      buttongroup.style.display = 'none';
    })
  </script>
</body>

</html>