## About Foodbank Processor

This backend for processing Foodbank.Digital

### Changelog

-   Upload/Download
-   Csv,Xls,Xlsx support

### Running queue

-   ./vendor/bin/sail artisan queue:work
